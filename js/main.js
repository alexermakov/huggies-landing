(function ($) {
  $.fn.inputFilter = function (inputFilter) {
    return this.on("input keydown keyup mousedown mouseup select contextmenu drop", function () {
      if (inputFilter(this.value)) {
        this.oldValue = this.value;
        this.oldSelectionStart = this.selectionStart;
        this.oldSelectionEnd = this.selectionEnd;
      } else if (this.hasOwnProperty("oldValue")) {
        this.value = this.oldValue;
        this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
      } else {
        this.value = "";
      }
    });
  };
}(jQuery));


$(function () {

  $('.js_field_required').on('keyup', function () {
    let validate = true;
    let form = $(this).parents('form');
    form.find('.js_field_required').each(function () {
      if ($(this).val() == '') validate = false;
    })
    form.find("input[type='submit']").prop('disabled', !validate);
  })


  $('.js_form_share_hugs').submit(function (e) {

    if ($(this)[0].checkValidity()) {
      $.fancybox.close();
      $.fancybox.open({src: '#modal_thanks'})
    }
  });



  $(".js_field_integer").inputFilter(function (value) {
    return /^\d*$/.test(value);
  });

  $('.js_btn_menu,.js_btn_modal_menu_close').click(function (e) {
    e.preventDefault();
    $('.js_modal_menu').toggleClass('active')
  });
  $('.js_ancor_wrap a').click(function (e) {
    e.preventDefault();
    $('.js_modal_menu').removeClass('active')
    $("html, body").animate({
      scrollTop: $($(this).attr('href')).offset().top - 40
    }, 1000);
  });

  $('.js_faq_btn').click(function (event) {
    $(this).parents('.faq_item').toggleClass('active');
    $(this).parents('.faq_item').find('.faq_item_info').slideToggle(400);
});

});